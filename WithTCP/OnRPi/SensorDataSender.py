import threading, random, time,json, socket, pprint, sys, ReadSensor, re,traceback

#this class will send the sensor data periodically to the server with predefined ip address and port, does nothing if fails
class SensorDataSender(threading.Thread):
	ServerIpAddr = "192.168.1.1"
	ServerPort = 2323
	BUFFER_SIZE = 500
	StopFlag = False
	def __init__(self, DataReaderObj):
		threading.Thread.__init__(self)
		self.DataReaderObj = DataReaderObj
		self.socket_using = None
		
	def run(self):
		while not self.StopFlag:
			try:
				msg = self.DataReaderObj.getData()
				#if self.socket_using is None:
				if True:
					self.socket_using = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
					self.socket_using.connect((self.ServerIpAddr, self.ServerPort))
				self.socket_using.send(msg)
				print "sent " + msg
				data = self.socket_using.recv(self.BUFFER_SIZE)
				print "receive " + data
				#expecting a struct like this 
				#{	"command" : "set_gio", 
					#"sensors" : {
						#"3" : { "0": 0, "1" : 1, "2" : 1, "3" : 1}
						#"4" : { "0": 0, "1" : 1, "2" : 1, "3" : 1}
					#}
				#}
				
				if re.search("set_gio", data):
					self.DataReaderObj.set_gio(json.loads(data))
					
				#expecting a struct like this 
				#{	"command" : "set_on_off", 
					#"value" : 0 for off and 1 for on
				#}	
				elif re.search("set_on_off", data):
					self.DataReaderObj.set_on_off(json.loads(data))	
				
			except:
				e = sys.exc_info()[0]
				print "Error" + str(e)
				print '-'*60
				traceback.print_exc(file=sys.stdout)
				print '-'*60
			finally:
				time.sleep(2)
			
	def stop(self):
		self.StopFlag = True
	
if __name__ == "__main__":
	try:		
		ReadSensorObj = ReadSensor.ReadSensor()
		ReadSensorObj.start()
	
		#active the client which will send data periodically to the server
		DataSender = SensorDataSender(ReadSensorObj)
		DataSender.start()
		
		#wait for 2 threads to be done, which they won't bc of infinite loop
		while True:
			pass 
	except KeyboardInterrupt:	
		DataSender.stop()
		ReadSensorObj.stop()
		
