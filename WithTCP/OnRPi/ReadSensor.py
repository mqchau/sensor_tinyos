import serial,threading, re, json, time, argparse, random, os
from collections import deque

 
DEBUG = True
  
class ReadSensor(threading.Thread):
	DEVICE = '/dev/ttyUSB0'
	BAUD_RATE = 57600
	GioSequenceInterval = 2.0				#5sec between each stage in sequence
	max_return_array_length = 5
	
	SerialPortLock = threading.RLock()
	SerialPortFlag = False
	
	StopFlagLock = threading.RLock()
	StopFlag = False
	
	DataLock = threading.RLock()
	DataFile = dict()
	
	GioSequenceLock = threading.RLock()
	GioSequenceOnOffFlag = False
	GioSequenceFirstFlag = False
	
			
	def __init__(self):
		threading.Thread.__init__(self)		
		self.current_sequence_state = 0
		self.clearAllData()
		
	def __exit__(self):
		threading.Thread.__exit__(self)
		self.serial_port.close()
		self.clearAllData()
	
	def clearAllData(self):
		#clear all data
		for one in self.DataFile.keys():
			self.clearData(one)
			
	def appendData(self, data_struct):
		with self.DataLock:
			if data_struct['sensorid'] not in self.DataFile:
				self.DataFile[data_struct['sensorid']] = {
						'light': 	deque()
					,	'temp': 	deque()
					,	'humid':	deque()
				}
		
			for keyword in self.DataFile[data_struct['sensorid']].keys():
				if len(self.DataFile[data_struct['sensorid']][keyword]) >= self.max_return_array_length:
					self.DataFile[data_struct['sensorid']][keyword].popleft()
				self.DataFile[data_struct['sensorid']][keyword].append(data_struct[keyword])	
			
	def clearData(self, sensorid):
		with self.DataLock:
			for array in self.DataFile[sensorid].values():
				array.clear()
			

	def run(self):
		#wait for telosb sensor to connect
		while not os.path.exists(self.DEVICE):
			print "waiting"
			time.sleep(1)
		#occupy the port
		print "connecting to serial port"
		self.serial_port = serial.Serial(self.DEVICE, self.BAUD_RATE, timeout=5)	
		while not self.checkStopFlag():
			#read data
			line = ''
			while len(line) == 0 and not self.checkStopFlag():
				with self.SerialPortLock:
					line = self.serial_port.readline().strip()
					self.checkGioSequence()
			#parse it
			data_struct = self.parseSensorString(line)
			
			if data_struct is not None:
				# if DEBUG:
					# print json.dumps(data_struct)
				
				#put them in array
				self.appendData(data_struct)
	
	def checkGioSequence(self):
		#over here we already have SerialPortLock already
		#hard code that only sensor 6 will be on and off
		with self.GioSequenceLock:
			if self.GioSequenceOnOffFlag:
				if time.time() - self.start_sequence_time > self.GioSequenceInterval or self.GioSequenceFirstFlag:
					self.GioSequenceFirstFlag = False
					gio0_val, gio1_val, gio2_val = self.get_next_sequence_state()
					string_to_send = ("GioConfig:NodeID=0006,GIO0=%d,GIO1=%d,GIO2=%d,GIO3=0___________________________________________" % (gio0_val,gio1_val,gio2_val))
					self.serial_port.write(string_to_send)
					self.start_sequence_time = time.time() + self.GioSequenceInterval
			else:
				string_to_send = "GioConfig:NodeID=0006,GIO0=0,GIO1=0,GIO2=0,GIO3=0___________________________________________"
				self.serial_port.write(string_to_send)
				
	def get_next_sequence_state(self):
		next_state = random.randrange(7) + 1
		while next_state == self.current_sequence_state:
			next_state = random.randrange(7) + 1
		self.current_sequence_state = next_state
		return self.convert_state_to_gio()
		
	def convert_state_to_gio(self):
		temp_current_sequence_state = self.current_sequence_state
		print "current_sequence_state = " + str(self.current_sequence_state)
		gio0 = temp_current_sequence_state % 2
		gio1 = (temp_current_sequence_state >> 1) % 2
		gio2 = (temp_current_sequence_state >> 2) % 2
		return gio0, gio1, gio2
		
	def checkStopFlag(self):
		with self.StopFlagLock:
			LocalStopFlag = self.StopFlag
		return LocalStopFlag
		
	def stop(self):
		with self.StopFlagLock:
			self.StopFlag = True
			
	def parseSensorString(self, string):
		if re.search(r'SensorID=(\d+),', string) and re.search(r'Light=(\d+),', string) and re.search(r'Temp=(\d+),', string) and re.search(r'Humid=(\d+),', string):
			sensorid_value = int(re.search(r'SensorID=(\d+),', string).group(1))
			light_value = int(re.search(r'Light=(\d+),', string).group(1))
			temp_value = int(re.search(r'Temp=(\d+),', string).group(1))
			humid_value = int(re.search(r'Humid=(\d+),', string).group(1))
			return {
				'sensorid' : sensorid_value,
				'light' : light_value,
				'temp' : temp_value,
				'humid' : humid_value
				}	
		else:
			return None
			
	def convertData(self):
		return_struct = dict()
		with self.DataLock:
			for one_id in self.DataFile:
				return_struct[one_id] = dict()
				for one_keyword in self.DataFile[one_id]:
					return_struct[one_id][one_keyword] = list(self.DataFile[one_id][one_keyword])
					
		return return_struct			
	
 	def getData(self):		
		return_string = json.dumps(self.convertData())		
		self.clearAllData()
		return return_string	

	def set_gio(self, gio_struct):
		#expecting a struct like this 
		#{	"command" : "set_gio", 
			#"sensors" : {
				#"3" : { "0": 0, "1" : 1, "2" : 1, "3" : 1}
				#"4" : { "0": 0, "1" : 1, "2" : 1, "3" : 1}
			#}
		#}
		#construct a string to send to serial port like this
		#GioConfig:NodeID=0003,GIO0=0,GIO1=0,GIO2=1,GIO3=1___________________________________________
		# print json.dumps(gio_struct)
		for one_id in gio_struct['sensors']:
			
			if (int(one_id) != 3 and int(one_id) != 4 and int(one_id) != 5):
				print "only support id 3 and 4 and 5"
			else:
				valid_data_flag = True
				for i in xrange(4):
					if gio_struct['sensors'][str(one_id)][str(i)] != 0 and gio_struct['sensors'][str(one_id)][str(i)] != 1 :
						print "only support gio value 0 and 1"
						valid_data_flag = False
				if valid_data_flag:			
					string_to_send = "GioConfig:NodeID=000%d,GIO0=%d,GIO1=%d,GIO2=%d,GIO3=%d___________________________________________" % (int(one_id), gio_struct['sensors'][str(one_id)]['0'], gio_struct['sensors'][str(one_id)]['1'], gio_struct['sensors'][str(one_id)]['2'], gio_struct['sensors'][str(one_id)]['3'])
					#send over serial port
					with SerialPortLock:
						self.serial_port.write(string_to_send)
	
	def set_on_off(self, on_off_struct):
		#expecting a struct like this 
			#{	"command" : "set_on_off", 
				#"value" : 0 for off and 1 for on
			#}
		with self.GioSequenceLock:
			if on_off_struct["value"] == 0:
				self.GioSequenceOnOffFlag = False
			else:
				if not self.GioSequenceOnOffFlag:
					self.start_sequence_time = time.time()
					self.GioSequenceFirstFlag = True
				self.GioSequenceOnOffFlag = True
				
	
	def test1(self):
		#this test will continuously read and sometimes do a write
		sensor.start()
		sample_gio_struct = {	"command" : "set_gio", 
				"sensors" : {
					"3" : { "0": 0, "1" : 1, "2" : 1, "3" : 1},
					"4" : { "0": 0, "1" : 1, "2" : 1, "3" : 1}
				}
			}
		gio_on_off = 0	
		for i in xrange(100):
			print self.getData()
			if i % 5 == 0:
				gio_on_off = random.randrange(2)
			sample_gio_struct["sensors"]["3"]["3"] = gio_on_off
			sample_gio_struct["sensors"]["3"]["2"] = gio_on_off
			sample_gio_struct["sensors"]["3"]["1"] = gio_on_off
			sample_gio_struct["sensors"]["3"]["0"] = gio_on_off
			self.set_gio(sample_gio_struct)
			print ('set_gio i = %d, val = %d' % (i, gio_on_off))
			time.sleep(1)
		sensor.stop()
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Select debug option')
	parser.add_argument('--debug', type=int, help='debug option')
	
	args = parser.parse_args()	
	sensor = ReadSensor()
	
	if args.debug == 1:
		#typical usage
		print "option debug 1"
		try:			
			sensor.start()
			while True:
				print sensor.getData()
				time.sleep(10)
		except KeyboardInterrupt:
			sensor.stop()
	elif args.debug == 2:
		#test read and write
		print "option debug 2"
		try:
			sensor.test1()
		except KeyboardInterrupt:
			sensor.stop()
			exit(1)
	else:
		print args.debug
