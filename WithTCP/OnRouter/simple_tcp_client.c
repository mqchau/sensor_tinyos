#include <stdio.h> 
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define SENSOR_COLLECTOR_ADDRESS 	"10.0.0.178"
#define SENSOR_COLLECTOR_PORT 		2324
#define BUFFER_LENGTH 				2000 
 
int get_data(char * return_buffer); 
 
int main(int argc , char *argv[])
{
	char return_buffer[BUFFER_LENGTH];
	memset(return_buffer, 0, sizeof(char) * BUFFER_LENGTH);
	int get_data_result = get_data(return_buffer);
	printf("status %d\n", get_data_result);
	printf("%s\n", return_buffer);
	return 0;
}

int get_data(char * return_buffer){
	int sock;
    struct sockaddr_in server;
    char server_reply[2000];
     
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    server.sin_addr.s_addr = inet_addr(SENSOR_COLLECTOR_ADDRESS);
    server.sin_family = AF_INET;
    server.sin_port = htons( SENSOR_COLLECTOR_PORT );
 
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }

	//Send some data
	if( send(sock, "get_data", 9 , 0) < 0)
	{
		puts("Send failed");
		return 1;
	}
	 
	//Receive a reply from the server
	if( recv(sock , return_buffer , 2000 , 0) < 0)
	{
		puts("recv failed");
		return 1;
	}
	
    close(sock);
    return 0;
}	